//! This is very specific implementation for evaluation purposes.

#![no_std]

use core::mem::MaybeUninit;
use bern_kernel::sync::Mutex;
use stm32f4xx_hal::i2c::I2c;
use stm32f4xx_hal::stm32::I2C1;
use stm32f4xx_hal::gpio::gpiob::{PB8, PB9};
use stm32f4xx_hal::gpio::{AlternateOD, AF4};
use embedded_hal::blocking::i2c::{WriteRead, Write};

type ExI2c = I2c<I2C1, (PB8<AlternateOD<AF4>>, PB9<AlternateOD<AF4>>)>;
#[link_section = ".shared"]
static mut BUS_I2C: Mutex<MaybeUninit<ExI2c>> = Mutex::new(MaybeUninit::uninit());

#[allow(dead_code)]
pub struct I2cMutex<'a> {
    mutex: &'a Mutex<MaybeUninit<ExI2c>>
}

impl  I2cMutex<'_>  {
    pub fn new(i2c: ExI2c) -> Self {
        unsafe {
            BUS_I2C = Mutex::new(MaybeUninit::new(i2c));
            BUS_I2C.register().ok();
            I2cMutex {
                mutex: &BUS_I2C
            }
        }
    }

    pub fn acquire_bus<'a>(&self) -> I2cBus<'a> {
        unsafe {
            I2cBus::new(&BUS_I2C)
        }
    }
}

#[derive(Copy, Clone)]
pub struct I2cBus<'a> {
    mutex: &'a Mutex<MaybeUninit<ExI2c>>
}

impl<'a> I2cBus<'a> {
    fn new(bus: &'a Mutex<MaybeUninit<ExI2c>>) -> Self {
        Self {
            mutex: bus,
        }
    }
}

impl WriteRead for I2cBus<'_> {
    type Error = stm32f4xx_hal::i2c::Error;

    fn write_read(&mut self, address: u8, bytes: &[u8], buffer: &mut [u8]) -> Result<(), Self::Error> {
        if let Ok(mut value) = self.mutex.lock(1000) {
            unsafe {
                (&mut *value.as_mut_ptr()).write_read(address, bytes, buffer)
            }
        } else {
            panic!("Bus access failed");
        }
    }
}

impl Write for I2cBus<'_> {
    type Error = stm32f4xx_hal::i2c::Error;

    fn write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        if let Ok(mut value) = self.mutex.lock(1000) {
            unsafe {
                (&mut *value.as_mut_ptr()).write(addr, bytes)
            }
        } else {
            panic!("Bus access failed");
        }
    }
}

unsafe impl Send for I2cBus<'_> {}