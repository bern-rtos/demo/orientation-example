#![deny(unsafe_code)]
#![no_main]
#![no_std]

extern crate panic_halt;

use cortex_m_rt::entry;
use stm32f4xx_hal as hal;
use crate::hal::{prelude::*, stm32};
use hal::time::KiloHertz;

use micromath::{F32Ext, vector, vector::F32x3};
use core::fmt::{self, Write, Display, Formatter};

use bern_kernel as kernel;

// custom sensor implementations
use lsm6dso::{Lsm6dso, SlaveAddr, Accelerometer, Gyro};
use stts751::{Stts751, Temperature};
use bern_kernel::task::{Task, Priority};
use bern_kernel::sync::{Mutex, Semaphore};

const DELAY_MS: u32 = 100;
const CLOCK_FREQUENCY: u32 = 48_000_000;

#[link_section = ".shared"]
static ACCELERATION: Mutex<Acceleration> = Mutex::new(Acceleration {
    acceleration: F32x3 { x: 0.0, y: 0.0, z: 0.0 },
    angular_velocity: F32x3 { x: 0.0, y: 0.0, z: 0.0 },
    angle: F32x3 { x: 0.0, y: 0.0, z: 0.0 },
});
#[link_section = ".shared"]
static ACC_READY: Semaphore = Semaphore::new(0);

#[link_section = ".shared"]
static TEMP: Mutex<Temp> = Mutex::new(Temp(0f32));
#[link_section = ".shared"]
static TEMP_READY: Semaphore = Semaphore::new(0);

#[link_section = ".shared"]
static SEND: Semaphore = Semaphore::new(0);

#[entry]
fn main() -> ! {
    // Take hardware peripherals
    let stm32_peripherals = stm32::Peripherals::take().expect("cannot take stm32 peripherals");

    // Set up the system clock. We want to run at 48MHz for this one.
    let rcc = stm32_peripherals.RCC.constrain();
    let clocks = rcc.cfgr.sysclk(CLOCK_FREQUENCY.hz()).freeze();

    // gpio's
    let gpioa = stm32_peripherals.GPIOA.split();
    let gpiob = stm32_peripherals.GPIOB.split();
    let gpioc = stm32_peripherals.GPIOC.split();

    // uart
    let txd = gpioa.pa2.into_alternate_af7();
    let rxd = gpioa.pa3.into_alternate_af7();
    let serial = hal::serial::Serial::usart2(
        stm32_peripherals.USART2,
        (txd, rxd),
        hal::serial::config::Config::default().baudrate(115_200.bps()),
        clocks
    ).unwrap();
    let (mut tx, _rx) = serial.split();

    // button and LEDs
    let mut led = gpioa.pa5.into_push_pull_output();
    led.set_high().ok();
    let _button = gpioc.pc13.into_floating_input();

    // init kernel
    kernel::sched::init();
    kernel::sched::set_tick_frequency(
        1_000,
        CLOCK_FREQUENCY
    );
    SEND.register().ok();
    TEMP.register().ok();
    TEMP_READY.register().ok();
    ACCELERATION.register().ok();
    ACC_READY.register().ok();

    // sensors
    let sda = gpiob.pb9.into_alternate_af4_open_drain();
    let scl = gpiob.pb8.into_alternate_af4_open_drain();
    let i2c = hal::i2c::I2c::i2c1(
        stm32_peripherals.I2C1,
        (scl, sda),
        KiloHertz(100),
        clocks
    );

    let i2c_bus = mutex_i2c::I2cMutex::new(i2c);

    Task::new()
        .priority(Priority(1))
        .static_stack(kernel::alloc_static_stack!(512))
        .spawn(move || {
            loop {
                led.set_high().ok();
                kernel::sleep(100);
                led.set_low().ok();
                kernel::sleep(900);
            }
        });

    let i2c = i2c_bus.acquire_bus();
    Task::new()
        .priority(Priority(1))
        .static_stack(kernel::alloc_static_stack!(4096))
        .spawn(move || {
            let mut sensor_imu = Lsm6dso::new(
                i2c,
                SlaveAddr::Alternate)
                .expect("LSM3DSO could not be initialized");
            sensor_imu.set_acc_range(lsm6dso::AccRange::G2).unwrap();
            sensor_imu.set_gyro_range(lsm6dso::GyroRange::DPS250).unwrap();

            let mut angle_gyro = F32x3::new(0., 0., 0.);
            loop {
                let acceleration = sensor_imu.accel_norm().unwrap();
                let angular_velocity = sensor_imu.gyro_norm().unwrap();
                let angle = calc_angles(&acceleration, &angular_velocity, &mut angle_gyro);

                if let Ok(mut value) = ACCELERATION.lock(1000) {
                    value.acceleration = acceleration;
                    value.angular_velocity = angular_velocity;
                    value.angle = angle;
                }

                ACC_READY.add_permits(1);
                SEND.add_permits(1);

                kernel::sleep(DELAY_MS);
            }
        });


    let i2c = i2c_bus.acquire_bus();
    Task::new()
        .priority(Priority(2))
        .static_stack(kernel::alloc_static_stack!(2048))
        .spawn(move || {
            let mut sensor_temp = Stts751::new(i2c)
                .expect("STTS751 could not be initialized");

            loop {
                let temp = sensor_temp.temp_norm().unwrap();
                //let temp = 42;
                match TEMP.lock(1000) {
                    Ok(mut value) => {
                        value.0 = temp;
                        TEMP_READY.add_permits(1);
                        SEND.add_permits(1);
                    }
                    Err(_) => {}
                }
                kernel::sleep(500);
            }
        });

    Task::new()
        .priority(Priority(3))
        .static_stack(kernel::alloc_static_stack!(2048))
        .spawn(move || {
            loop {
                if let Ok(permit) = SEND.acquire(1000) {
                    if let Ok(permit) = TEMP_READY.try_acquire() {
                        if let Ok(value) = TEMP.try_lock() {
                            writeln!(tx, "{}", *value).unwrap();
                        }
                        permit.forget();
                    }

                    if let Ok(permit) = ACC_READY.try_acquire() {
                        if let Ok(value) = ACCELERATION.try_lock() {
                            // print measurements and orientation in JSON format
                            writeln!(tx, "{}", *value).unwrap();
                        }
                        permit.forget();
                    }

                    permit.forget();
                }
            }
        });

    kernel::sched::start();
}

pub struct Acceleration {
    pub acceleration: F32x3,
    pub angular_velocity: F32x3,
    pub angle: F32x3,
}
impl Display for Acceleration {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f,
           "{{\
               \"meas\":\"acc\",\
               \"values\":{{\
                   \"x\":{},\
                   \"y\":{},\
                   \"z\":{}\
               }},\
               \"unit\":\"g\"\
           }}\n\
           {{\
                \"meas\":\"gyro\",\
                \"values\":{{\
                    \"x\":{},\
                    \"y\":{},\
                    \"z\":{}\
                }},\
                \"unit\":\"dps\"\
           }}\n\
           {{\
                \"meas\":\"angle\",\
                \"values\":{{\
                    \"x\":{},\
                    \"y\":{},\
                    \"z\":{}\
                }},\
                \"unit\":\"°\"\
            }}",
            self.acceleration.x,
            self.acceleration.y,
            self.acceleration.z,
            self.angular_velocity.x,
            self.angular_velocity.y,
            self.angular_velocity.z,
            self.angle.x,
            self.angle.y,
            self.angle.z
        )
    }
}


pub struct Temp(pub f32);
impl Display for Temp {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f,
               "{{\
                   \"meas\":\"temp\",\
                   \"values\":{{\
                       \"T\":{}\
                   }},\
                   \"unit\":\"°C\"\
               }}",
               self.0)
    }
}


/// calculate 3D orientation on degrees from angular velocity (°/s) and
/// acceleration (g)
fn calc_angles(acc: &F32x3, gyro: &F32x3, angle_gyro: &mut F32x3) -> vector::F32x3 {
    const TIME_STEP: f32 = DELAY_MS as f32 /1000.;
    const DEG_PER_RAD: f32 = 57.2958;
    let beta = 0.05;

    let mut angle_acc = F32x3::new(0., 0., 0.);
    angle_acc.x = - F32Ext::atan2(
        acc.y,
        F32Ext::sqrt(acc.x.powi(2) + acc.z.powi(2))
    ) * DEG_PER_RAD;
    angle_acc.y = 90. - F32Ext::atan2(
        acc.z,
        F32Ext::sqrt(acc.x.powi(2) + acc.y.powi(2))
    ) * DEG_PER_RAD;
    angle_acc.z = F32Ext::atan2(
        acc.x,
        F32Ext::sqrt(acc.y.powi(2) + acc.z.powi(2))
    ) * DEG_PER_RAD;

    angle_gyro.x += -gyro.x * TIME_STEP;
    angle_gyro.y += -gyro.y * TIME_STEP;
    angle_gyro.z += gyro.z * TIME_STEP;

    // calculate weighted angle from the two sensors
    F32x3::new(
        angle_acc.x * beta + (1.-beta) * angle_gyro.x,
        angle_acc.y * beta + (1.-beta) * angle_gyro.y,
        angle_acc.z * beta + (1.-beta) * angle_gyro.z,
    )
}
